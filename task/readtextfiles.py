# read all .txt files in the directory
import csv
import glob
import errno
path = '/home/suk/Desktop/mls/task/pdfs/*.txt'
files = glob.glob(path)

for name in files:
    try:
        with open(name) as f:
            for line in f:
                data = line.split(': ')
                print(data)
                with open("output.csv",'w') as resultFile:
                    wr = csv.writer(resultFile, dialect='excel')
                    wr.writerow(data)
    except IOError as exc:
        if exc.errno != errno.EISDIR:
            raise

